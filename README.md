# India Water Portal visualization

## Description

This is to visualize the data from India Water Portal, partnership with Viz for Social Good (see [docs](docs/IndiaWaterPortal_Arghyam-V4SG.pdf)). However, I did not submit my visualizations as map graphing (`folium` + `altair`) is new to me.

## Requirements

This is done in `jupyterlab` with `python3`. For detailed requirements, please see [`requirements.txt`](requirements.txt).

## Data sources

### India Water Portal

Please see the file [IndiaWaterPortal_Arghyam-V4SG.pdf](docs/IndiaWaterPortal_Arghyam-V4SG.pdf) for more information

### India GIS

The GIS files were downloaded from <https://www.diva-gis.org/gdata> from a zip file, and extract into `gis` folder

```bash
wget https://biogeo.ucdavis.edu/data/diva/adm/IND_adm.zip
unzip IND_adm.zip -d gis
```

This is the content of the included `license.txt` file:

> These data were extracted from the GADM database (www.gadm.org), version 2.5, July 2015. They can be used for non-commercial purposes only. It is not allowed to redistribute these data, or use them for commercial purposes, without prior consent.

## Notebooks

In [`notebooks`](notebooks) (because Codeberg can't yet render jupyter notebooks, I'm going to also attach the corresponding [`nbviewer`](https://nbviewer.org) for viewing):

- Run [`preprocess.ipynb`](https://nbviewer.org/urls/codeberg.org/penguinsfly/v4sg-iwp-2022/raw/branch/main/notebooks/preprocess.ipynb) for preprocessing and cleaning.
- Then run [`visualize.ipynb`](https://nbviewer.org/urls/codeberg.org/penguinsfly/v4sg-iwp-2022/raw/branch/main/notebooks/visualize.ipynb) to generate visualization, saved in [`figures`](figures)

## Figures

- The generated `.pdf` figures can be viewed inside the [`figures`](figures) folder or the [`visualize.ipynb`](https://nbviewer.org/urls/codeberg.org/penguinsfly/v4sg-iwp-2022/raw/branch/main/notebooks/visualize.ipynb) notebook.
- The generated `.html` interactive maps are:
  - [Non-monsoon rainfall changes](https://penguinsfly.codeberg.page/v4sg-iwp-2022/figures/state_rainfall_map.html)
  - [Development status and Water quality](https://penguinsfly.codeberg.page/v4sg-iwp-2022/figures/dev-status_and_water-quality_map.html)
