# Useful links about `folium`

- <https://medium.com/swlh/interactive-choropleth-maps-in-python-dd943b99df50>
- <https://livecodestream.dev/post/how-to-plot-your-data-on-maps-using-python-and-folium/>
- <https://towardsdatascience.com/folium-and-choropleth-map-from-zero-to-pro-6127f9e68564>
- <https://towardsdatascience.com/the-battle-of-choropleths-part-3-folium-86ab1232afc>
- <https://towardsdatascience.com/how-to-embed-interactive-plotly-visualizations-in-folium-map-pop-ups-c69c818a8cd9>
- <https://stackoverflow.com/questions/57314597/folium-choropleth-map-is-there-a-way-to-add-crosshatching-for-nan-values>
- <https://nbviewer.org/gist/BibMartin/f153aa957ddc5fadc64929abdee9ff2e>
- <https://www.wenyandeng.net/blog/folium-setting-up-color-scales>
- <https://vialab.mit.edu/tutorials/module/mapping-in-python-folium/>
- <https://stackoverflow.com/questions/51961848/set-color-for-missing-values-in-folium-choropleth>
- <https://stackoverflow.com/questions/72222835/folium-put-markers-in-marker-clusters-and-in-layers-based-on-a-value>
- <https://stackoverflow.com/questions/61698593/folium-0-11-0-keep-markers-layer-in-front>