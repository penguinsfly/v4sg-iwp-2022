# GIS data 

The GIS files were downloaded from <https://www.diva-gis.org/gdata> from a zip file.

```bash
# if inside the `gis` folder
wget https://biogeo.ucdavis.edu/data/diva/adm/IND_adm.zip
unzip IND_adm.zip
```

This is the content of the included `license.txt` file:

> These data were extracted from the GADM database (www.gadm.org), version 2.5, July 2015. They can be used for non-commercial purposes only.  It is not allowed to redistribute these data, or use them for commercial purposes, without prior consent.
